%% Image processing
clc; clear all; close all
myDir = 'C:\Users\Kuo\Desktop\combustion lab';
myFiles = dir(fullfile(myDir,'*.jpg')); 

for i = 1:length(myFiles);
q = myFiles(i,1).name;

output = exifread(q);

UC = output.UserComment;

d.name = q;
d.a_2 = UC(37:40);
d.rho_2 = UC(57:61);
d.V2 = UC(73:78);
d.T2 = UC(88:91);
d.T_f = UC(105:108);
d.P = UC(118:124);
d.Y_O2 = UC(132:137);
d.Y_N2 = UC(145:150);
d.FF = UC(177:181);
d.CFF = UC(206:209);
d.COF = UC(238:241);

D(i) = d;

I=imread(q);
RGB=rgb2gray(I);
C=corner(RGB, 'Harris', 4);
I2=imcrop(RGB,C);
imshow(I2);

figure
end

%% Failed:
%     filename = sprintf('IMG_%.4d.jpg',i);
%     BaseName = 'C:\Users\Roel\Dropbox\InternshipUCSD\IMG_000'
%     FileName = imread(BaseName,num2str(i))
%     a = imread('C:\Users\Roel\Dropbox\InternshipUCSD\q.jpg');

% output = exifread('C:\Users\Roel\Dropbox\InternshipUCSD\q.jpg') 
% date = UC(1:19);
% date = sprintf(UC,'%19.5s');
% date2 = strread(sprintf('%s',UC),'%1c%*[^\n]') ;
% date3 = textscan(UC,'%19c');
% d.a_2     = sscanf(UC,'date PM  a_2 (1/s):%f') ;
% d.rho_2   = sscanf(UC,'2013-05-08 02:31:22 PM  a_2 (1/s):  60  rho_2 (kg/m^3):%f');
% d.V2      = sscanf(UC,'2013-05-08 02:31:22 PM  a_2 (1/s):  60  rho_2 (kg/m^3): 1.99  V2 (m/s):%f') ;
% d.T2      = sscanf(UC,'2013-05-08 02:31:22 PM  a_2 (1/s):  60  rho_2 (kg/m^3): 1.99  V2 (m/s): 0.300  T2 (K):%f') ;
% d.T_fuel  = sscanf(UC,'2013-05-08 02:31:22 PM  a_2 (1/s):  60  rho_2 (kg/m^3): 1.99  V2 (m/s): 0.300  T2 (K): 298  T_Fuel (K):%f') ;
% d.P       = sscanf(UC,'2013-05-08 02:31:22 PM  a_2 (1/s):  60  rho_2 (kg/m^3): 1.99  V2 (m/s): 0.300  T2 (K): 298  T_Fuel (K): 298  P (Pa):%f') ;
% d.Y_02    = sscanf(UC,'2013-05-08 02:31:22 PM  a_2 (1/s):  60  rho_2 (kg/m^3): 1.99  V2 (m/s): 0.300  T2 (K): 298  T_Fuel (K): 298  P (Pa): 174033  Y_O2:%f');
% d.Y_N2    = sscanf(UC,'2013-05-08 02:31:22 PM  a_2 (1/s):  60  rho_2 (kg/m^3): 1.99  V2 (m/s): 0.300  T2 (K): 298  T_Fuel (K): 298  P (Pa): 174033  Y_O2: 0.175  Y_N2:%f');
% d.FFR     = sscanf(UC,'2013-05-08 02:31:22 PM  a_2 (1/s):  60  rho_2 (kg/m^3): 1.99  V2 (m/s): 0.300  T2 (K): 298  T_Fuel (K): 298  P (Pa): 174033  Y_O2: 0.175  Y_N2: 0.825  Fuel Flowrate (mL/min):%f');
% d.CFF     = sscanf(UC,'2013-05-08 02:31:22 PM  a_2 (1/s):  60  rho_2 (kg/m^3): 1.99  V2 (m/s): 0.300  T2 (K): 298  T_Fuel (K): 298  P (Pa): 174033  Y_O2: 0.175  Y_N2: 0.825  Fuel Flowrate (mL/min):  2.85  Curtain Fuel Fraction:%f');
% d.COF     = sscanf(UC,'2013-05-08 02:31:22 PM  a_2 (1/s):  60  rho_2 (kg/m^3): 1.99  V2 (m/s): 0.300  T2 (K): 298  T_Fuel (K): 298  P (Pa): 174033  Y_O2: 0.175  Y_N2: 0.825  Fuel Flowrate (mL/min):  2.85  Curtain Fuel Fraction: 0.1  Curtain Oxidizer Fraction:%f')