function d = parseExifData(imageFilePath)

output = exifread(imageFilePath);

UC = output.UserComment;

d.name = q;
d.a_2 = UC(37:40);
d.rho_2 = UC(57:61);
d.V2 = UC(73:78);
d.T2 = UC(88:91);
d.T_f = UC(105:108);
d.P = UC(118:124);
d.Y_O2 = UC(132:137);
d.Y_N2 = UC(145:150);
d.FF = UC(177:181);
d.CFF = UC(206:209);
d.COF = UC(238:241);
